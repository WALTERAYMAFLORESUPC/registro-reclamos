package pe.gob.minedu.perueduca.users.application.service;


import org.springframework.stereotype.Service;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.ReclamoEntity;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.ReclamoEntity;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.TbUbigeo;
import pe.gob.minedu.perueduca.users.application.reclamo.repository.ReclamoRepository;
import pe.gob.minedu.perueduca.users.application.reclamo.repository.TbUbigeoRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ReclamoService {
    TbUbigeoRepository tbUbigeoRepository;
    ReclamoRepository reclamoRepository;
    public ReclamoService(ReclamoRepository reclamoRepository, TbUbigeoRepository tbUbigeoRepository){
        this.reclamoRepository=reclamoRepository;
        this.tbUbigeoRepository=tbUbigeoRepository;

    }

    public ReclamoEntity guardarReclamo(ReclamoEntity reclamo){
        System.out.println("reclamo: " + reclamo.toString());
        return reclamoRepository.save(reclamo);
    }

    public ArrayList<ReclamoEntity> getReclamos(){
        return (ArrayList<ReclamoEntity>) reclamoRepository.findAll();

    }

    public ArrayList<ReclamoEntity> getReclamosPorFecha(Date fecha_inicio, Date fecha_fin){
        ArrayList <ReclamoEntity> reclamos_= reclamoRepository.findByFecha(fecha_inicio, fecha_fin );
        ArrayList <ReclamoEntity> reclamos= new ArrayList<>();
        for(ReclamoEntity obj : reclamos_){
            System.out.println("obj: " + obj.getUbigeo());
            Optional<TbUbigeo> distrito = getDistritoPorId(obj.getUbigeo());
            System.out.println("distrito:" + distrito);
            System.out.println("ubigeo_desc:" + distrito.get().getDistrito());
            obj.setUbigeo_desc(distrito.get().getDistrito());
            reclamos.add(obj);
        }
        return reclamos;
    }
    public ArrayList<ReclamoEntity> getReclamosPorDni(int dni){
        ArrayList <ReclamoEntity> reclamos_= reclamoRepository.findByByDni(dni);
        ArrayList <ReclamoEntity> reclamos= new ArrayList<>();
        for(ReclamoEntity obj : reclamos_){
            System.out.println("obj: " + obj.getUbigeo());
            Optional<TbUbigeo> distrito = getDistritoPorId(obj.getUbigeo());
            System.out.println("distrito:" + distrito);
            System.out.println("ubigeo_desc:" + distrito.get().getDistrito());
            obj.setUbigeo_desc(distrito.get().getDistrito());
            reclamos.add(obj);
        }
        return reclamos;
    }

    public ArrayList<ReclamoEntity> getReclamosPorDistrito(String distrito){
        return (ArrayList<ReclamoEntity>) reclamoRepository.findByByDistrito(distrito);
    }

    public ArrayList<TbUbigeo> getRegiones(){
        return tbUbigeoRepository.findAllGroupByIdRegion();
    }
    public ArrayList<TbUbigeo> getProvinciasPorRegion(String id_region){
        return tbUbigeoRepository.findAllProvinciaGroupById(id_region);
    }
    public ArrayList<TbUbigeo> getDistritosPorProvincia(String id_provincia){
        return tbUbigeoRepository.findAllDistritoGroupById(id_provincia);
    }
    public Optional<TbUbigeo> getDistritoPorId(String idDistrito){
        System.out.println("idDistrito:::" + idDistrito);
        return tbUbigeoRepository.findDistritoById(idDistrito);
    }








}
