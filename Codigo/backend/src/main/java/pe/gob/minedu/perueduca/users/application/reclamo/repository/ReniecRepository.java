package pe.gob.minedu.perueduca.users.application.reclamo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.Reniec;


@Repository
public interface ReniecRepository extends JpaRepository<Reniec, Integer> {


    @Query(value = "SELECT * FROM reniec where dni=:dni and ubigeo=:ubigeo", nativeQuery = true)
    Reniec findByByDniAndUbigeo(@Param("dni") int dni, @Param("ubigeo") String ubigeo);


}
