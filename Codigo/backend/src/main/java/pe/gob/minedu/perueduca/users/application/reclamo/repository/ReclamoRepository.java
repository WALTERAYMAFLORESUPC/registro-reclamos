package pe.gob.minedu.perueduca.users.application.reclamo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.ReclamoEntity;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.ReclamoEntity;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.Reniec;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;


/**
 * @author Rony Villanueva <rony@vimero.io>
 * Created on 25/05/2020
 */

@Repository
public interface ReclamoRepository extends JpaRepository<ReclamoEntity, Integer> {

    @Query(value = "SELECT * FROM reclamo where fecha between :fecha_inicio and :fecha_fin", nativeQuery = true)
    ArrayList<ReclamoEntity>findByFecha(@Param("fecha_inicio") Date fecha_inicio, @Param("fecha_fin") Date fecha_fin);

    @Query(value = "SELECT * FROM reclamo where numero_documento=:dni", nativeQuery = true)
    ArrayList<ReclamoEntity>findByByDni(@Param("dni") int dni);

    @Query(value = "SELECT * FROM reclamo where ubigeo=:ubigeo", nativeQuery = true)
    ArrayList<ReclamoEntity>findByByDistrito(@Param("ubigeo") String distrito);

}
