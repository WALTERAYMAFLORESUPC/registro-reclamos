package pe.gob.minedu.perueduca.users.application.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pe.gob.minedu.perueduca.users.application.jms.JmsReclamos;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.ReclamoEntity;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.Reniec;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.TbUbigeo;
import pe.gob.minedu.perueduca.users.application.service.ReclamoService;
import pe.gob.minedu.perueduca.users.application.service.ReniecService;
import pe.gob.minedu.perueduca.users.application.service.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping("/registro-reclamo")
@CrossOrigin(origins = {"*"})
public class RegistroReclamoController {

    private UserService userService;
    private ReniecService reniecService;
    private ReclamoService reclamoService;
    @Autowired
    private JmsReclamos jmsReclamos;
    String userId="";
    public RegistroReclamoController(UserService userService, ReniecService reniecService, ReclamoService reclamoService) {
        this.userService = userService;
        this.reniecService = reniecService;
        this.reclamoService = reclamoService;


    }

    @PostMapping("/save")
    public ReclamoEntity insertarReclamo(@RequestBody ReclamoEntity reclamo){
        //convertir a JSON
        String json=null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            json=mapper.writeValueAsString(reclamo);
            jmsReclamos.send(json);
        } catch (Exception e) {
            System.out.println("--Falló, no enviado.--");
            reclamo.setRespuesta("Falló, no enviado.");
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Ocurriò un error, intentalo de nuevo.");
        }

        return reclamo;
    }

    @GetMapping("/reclamos")
    public ArrayList<ReclamoEntity> getAllReclamos(){
        return reclamoService.getReclamos();
    }

    @GetMapping("/reclamos/fecha/{fecha_inicio}/{fecha_fin}")
    public ArrayList<ReclamoEntity> getReclamosPorDni(@PathVariable("fecha_inicio") String fecha_inicio, @PathVariable("fecha_fin") String fecha_fin) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = sdf.parse(fecha_inicio);
        Date parsed2 = sdf.parse(fecha_fin);
        return reclamoService.getReclamosPorFecha(parsed,parsed2);
    }
    @GetMapping("/reclamos/dni/{dni}")
    public ArrayList<ReclamoEntity> getReclamosPorDni(@PathVariable("dni") int dni) throws ParseException {
        return reclamoService.getReclamosPorDni(dni);
    }

    @GetMapping("/reclamos/distrito/{distrito}")
    public ArrayList<ReclamoEntity> getReclamosPorDni(@PathVariable("distrito") String distrito){
        return reclamoService.getReclamosPorDistrito(distrito);
    }

    @GetMapping("/regiones")
    public ArrayList<TbUbigeo> getRegiones(){
       return reclamoService.getRegiones();
    }

    @GetMapping("/provincias/{idRegion}")
    public ArrayList<TbUbigeo> getProvinciasPorRegion(@PathVariable("idRegion")String id_region){
        return reclamoService.getProvinciasPorRegion(id_region);
    }

    @GetMapping("/distritos/{idProvincia}")
    public ArrayList<TbUbigeo> getDistritosPorProvincia(@PathVariable("idProvincia")String id_provincia){
        return reclamoService.getDistritosPorProvincia(id_provincia);
    }

    @GetMapping("/reniec/{dni}/{ubigeo}")
    public Reniec getPersonaPorDniUbigeo(@PathVariable("dni") int dni, @PathVariable("ubigeo") String ubigeo){
        return reniecService.getPersona(dni, ubigeo);
    }




}
