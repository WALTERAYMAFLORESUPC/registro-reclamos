package pe.gob.minedu.perueduca.users.application.service;


import org.springframework.stereotype.Service;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.Reniec;
import pe.gob.minedu.perueduca.users.application.reclamo.repository.ReniecRepository;

@Service
public class ReniecService {

    ReniecRepository reniecRepository;
    public ReniecService(ReniecRepository reniecRepository){
        this.reniecRepository=reniecRepository;

    }

    public Reniec getPersona(int dni, String ubigeo){
        return reniecRepository.findByByDniAndUbigeo(dni, ubigeo);
    }










}
