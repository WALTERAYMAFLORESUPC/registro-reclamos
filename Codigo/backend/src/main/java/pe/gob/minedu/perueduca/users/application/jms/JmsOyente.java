package pe.gob.minedu.perueduca.users.application.jms;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.ReclamoEntity;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.Reniec;
import pe.gob.minedu.perueduca.users.application.service.ReclamoService;
import pe.gob.minedu.perueduca.users.application.service.ReniecService;

import java.util.Date;

@Component
public class JmsOyente {
    @Autowired
    private ReclamoService reclamoService;
    @Autowired
    private ReniecService reniecService;
    @JmsListener(destination = "${jms.queue.destination}")
    public void miMensaje(String mensajeJSON){

        ObjectMapper mapper= new ObjectMapper();
        try {
            ReclamoEntity reclamo= mapper.readValue(mensajeJSON, ReclamoEntity.class);

            Reniec persona=reniecService.getPersona(reclamo.getNumero_documento(), reclamo.getUbigeo_());
            String ape_pat=persona.getApe_pat();
            String ape_mat=persona.getApe_mat();
            String nombres=persona.getNombres();
            reclamo.setFecha(new Date());
            reclamo.setApe_pat(ape_pat);
            reclamo.setApe_mat(ape_mat);
            reclamo.setNombres(nombres);
            reclamo.setRespuesta("Registraando en bd");

            System.out.println("mensajeJSON: "+ mensajeJSON);

            ReclamoEntity reclamo_insertado=reclamoService.guardarReclamo(reclamo);
                if(reclamo_insertado==null){
                    System.out.println("No se puedo registrar");
                }
                else{
                    System.out.println("Registro");
                }
            }

        catch (Exception e){
        System.out.println(e.getMessage());
        }

    }

}
