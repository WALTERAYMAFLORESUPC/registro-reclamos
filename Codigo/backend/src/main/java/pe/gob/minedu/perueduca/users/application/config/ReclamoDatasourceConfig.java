package pe.gob.minedu.perueduca.users.application.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;



@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "reclamoEntityManagerFactory",
        transactionManagerRef= "reclamoTransactionManager",
        basePackages = {"pe.gob.minedu.perueduca.users.application.reclamo.repository"})
public class ReclamoDatasourceConfig {

    @Bean
    @Primary
    @ConfigurationProperties("reclamo.datasource")
    public DataSourceProperties reclamoDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    @ConfigurationProperties("reclamo.datasource.configuration")
    public DataSource reclamoDataSource() {
        return reclamoDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }

    @Primary
    @Bean(name = "reclamoEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean reclamoEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(reclamoDataSource())
                .packages("pe.gob.minedu.perueduca.users.application.reclamo.entity")
                .build();
    }

    @Primary
    @Bean
    public PlatformTransactionManager reclamoTransactionManager(
            final @Qualifier("reclamoEntityManagerFactory") LocalContainerEntityManagerFactoryBean reclamoEntityManagerFactory) {
        return new JpaTransactionManager(reclamoEntityManagerFactory.getObject());
    }

}
