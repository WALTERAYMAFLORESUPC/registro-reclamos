package pe.gob.minedu.perueduca.users.application.reclamo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.TbUbigeo;

import java.util.ArrayList;
import java.util.Optional;


/**
 * @author Rony Villanueva <rony@vimero.io>
 * Created on 25/05/2020
 */

@Repository
public interface TbUbigeoRepository extends JpaRepository<TbUbigeo, String> {

    @Query(value = "SELECT a.id_region, a.region,cod_ubigeo_ie,distrito,id_provincia,provincia FROM tb_ubigeo a GROUP BY a.id_region\n", nativeQuery = true)
    ArrayList<TbUbigeo> findAllGroupByIdRegion();

    @Query(value = "SELECT a.id_region, a.region,cod_ubigeo_ie,distrito,id_provincia,provincia FROM tb_ubigeo a where id_region= :id_region GROUP BY a.id_provincia\n", nativeQuery = true)
    ArrayList<TbUbigeo> findAllProvinciaGroupById(@Param("id_region") String id_region);

    @Query(value = "SELECT a.id_region, a.region,cod_ubigeo_ie,distrito,id_provincia,provincia FROM tb_ubigeo a WHERE id_provincia= :id_provincia\n", nativeQuery = true)
    ArrayList<TbUbigeo> findAllDistritoGroupById(@Param("id_provincia") String id_provincia);

    @Query(value = "SELECT a.id_region, a.region,cod_ubigeo_ie,distrito,id_provincia,provincia FROM tb_ubigeo a WHERE a.cod_ubigeo_ie= :id_distrito\n", nativeQuery = true)
    Optional<TbUbigeo> findDistritoById(@Param("id_distrito") String id_distrito);
}
