package pe.gob.minedu.perueduca.users;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pe.gob.minedu.perueduca.users.application.service.UserService;

import pe.gob.minedu.perueduca.users.application.reclamo.repository.ReclamoRepository;


@Configuration
public class UsersConfiguration {



    private AmqpTemplate amqpTemplate;

    private ReclamoRepository userColasRepository;

    public UsersConfiguration(AmqpTemplate amqpTemplate, ReclamoRepository userColasRepository) {
        this.amqpTemplate = amqpTemplate;
        this.userColasRepository = userColasRepository;
    }





    @Bean
    public UserService getUserService(){
        return new UserService(userColasRepository);
    }

}
