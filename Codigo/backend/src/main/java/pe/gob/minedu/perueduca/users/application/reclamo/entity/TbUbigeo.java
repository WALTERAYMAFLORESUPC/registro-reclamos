package pe.gob.minedu.perueduca.users.application.reclamo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Rony Villanueva <rony@vimero.io>
 * Created on 25/05/2020
 */

@Entity
@Table(name = "tb_ubigeo")
public class TbUbigeo {

    @Column(name = "cod_ubigeo_ie")
    private String cod_ubigeo_ie;
    @Id
    @Column(name = "distrito")
    private String distrito;

    @Column(name = "id_provincia")
    private String id_provincia;

    @Column(name = "provincia")
    private String provincia;

    @Column(name = "id_region")
    private String id_region;

    @Column(name = "region")
    private String region;

    public String getCod_ubigeo_ie() {
        return cod_ubigeo_ie;
    }

    public void setCod_ubigeo_ie(String cod_ubigeo_ie) {
        this.cod_ubigeo_ie = cod_ubigeo_ie;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getId_provincia() {
        return id_provincia;
    }

    public void setId_provincia(String id_provincia) {
        this.id_provincia = id_provincia;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getId_region() {
        return id_region;
    }

    public void setId_region(String id_region) {
        this.id_region = id_region;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }



}
