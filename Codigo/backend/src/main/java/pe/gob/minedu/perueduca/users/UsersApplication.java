package pe.gob.minedu.perueduca.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import pe.gob.minedu.perueduca.users.application.reclamo.entity.ReclamoEntity;
import pe.gob.minedu.perueduca.users.application.reclamo.entity.TbUbigeo;
import pe.gob.minedu.perueduca.users.application.reclamo.repository.ReclamoRepository;
import pe.gob.minedu.perueduca.users.application.reclamo.repository.TbUbigeoRepository;
import pe.gob.minedu.perueduca.users.application.service.ReclamoService;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@SpringBootApplication
public class UsersApplication implements CommandLineRunner {

    @Autowired
    private ReclamoService reclamoService;
    @Autowired
    private ReclamoRepository reclamoRepository;
    @Autowired
    private TbUbigeoRepository tbUbigeoRepository;
    public static void main(String[] args) {
        SpringApplication.run(UsersApplication.class, args);
    }
    @Override
    public void run(String... args) throws Exception {

        ArrayList<TbUbigeo> tbUbigeo = tbUbigeoRepository.findAllGroupByIdRegion();

        for(TbUbigeo vd : tbUbigeo) {
            System.out.println("encontradaaaaaaas22222::::  " + vd.getId_region() + " - " + vd.getRegion());
        }
        ArrayList<TbUbigeo> tbUbigeo_prov = tbUbigeoRepository.findAllProvinciaGroupById("01");
        for(TbUbigeo vd : tbUbigeo_prov) {
            System.out.println("encontradaaaaaaas3::::  " + vd.getId_provincia() + " - " + vd.getProvincia());
        }

        List<ReclamoEntity> reclamos = reclamoRepository.findAll();
        for(ReclamoEntity reclamo : reclamos) {
            System.out.println("encontradaaaaaaas3::::  " + reclamo.getReclamo() + " - " + reclamo.getCorreo());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = sdf.parse("2020-08-28");
        Date parsed2 = sdf.parse("2020-08-28");
        List<ReclamoEntity> reclamosPorDni = reclamoService.getReclamosPorDni(70000001);
        for(ReclamoEntity reclamo : reclamosPorDni) {
            System.out.println("encontradaaaaaaas4::::  " + reclamo.getReclamo() + " - " + reclamo.getCorreo() + " - " + reclamo.getUbigeo_desc());
        }










    }
}
