package pe.gob.minedu.perueduca.users.application.reclamo.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Rony Villanueva <rony@vimero.io>
 * Created on 25/05/2020
 */

@Entity
@Table(name = "reclamo")
public class ReclamoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "tipo_cliente")
    private long tipo_cliente;

    @Column(name = "tipo_documento")
    private int tipo_documento;

    @Column(name = "numero_documento")
    private int numero_documento;

    @Column(name = "ape_pat")
    private String ape_pat;

    @Column(name = "ape_mat")
    private String ape_mat;

    @Column(name = "nombres")
    private String nombres;

    @Column(name = "correo")
    private String correo;

    @Column(name = "ubigeo")
    private String ubigeo;

    @Column(name = "motivo_reclamo")
    private String motivo_reclamo;

    @Column(name = "prioridad")
    private String prioridad;

    @Column(name = "reclamo")
    private String reclamo;

    @Column(name = "observaciones_comentarios")
    private String observaciones_comentarios;

    @Column(name = "fecha")
    private Date fecha;

    @Transient
    private String respuesta;

    @Transient
    private String ubigeo_;

    /*descripciones de id*/

    @Transient
    private String ubigeo_desc;
    @Transient
    private String motivo_reclamo_desc;
    @Transient
    private String prioridad_desc;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTipo_cliente() {
        return tipo_cliente;
    }

    public void setTipo_cliente(long tipo_cliente) {
        this.tipo_cliente = tipo_cliente;
    }

    public int getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(int tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public int getNumero_documento() {
        return numero_documento;
    }

    public void setNumero_documento(int numero_documento) {
        this.numero_documento = numero_documento;
    }

    public String getApe_pat() {
        return ape_pat;
    }

    public void setApe_pat(String ape_pat) {
        this.ape_pat = ape_pat;
    }

    public String getApe_mat() {
        return ape_mat;
    }

    public void setApe_mat(String ape_mat) {
        this.ape_mat = ape_mat;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    public String getMotivo_reclamo() {
        return motivo_reclamo;
    }

    public void setMotivo_reclamo(String motivo_reclamo) {
        this.motivo_reclamo = motivo_reclamo;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getReclamo() {
        return reclamo;
    }

    public void setReclamo(String reclamo) {
        this.reclamo = reclamo;
    }

    public String getObservaciones_comentarios() {
        return observaciones_comentarios;
    }

    public void setObservaciones_comentarios(String observaciones_comentarios) {
        this.observaciones_comentarios = observaciones_comentarios;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getUbigeo_() {
        return ubigeo_;
    }

    public void setUbigeo_(String ubigeo_) {
        this.ubigeo_ = ubigeo_;
    }

    public String getUbigeo_desc() {
        return ubigeo_desc;
    }

    public void setUbigeo_desc(String ubigeo_desc) {
        this.ubigeo_desc = ubigeo_desc;
    }

    public String getMotivo_reclamo_desc() {
        return motivo_reclamo_desc;
    }

    public void setMotivo_reclamo_desc(String motivo_reclamo_desc) {
        this.motivo_reclamo_desc = motivo_reclamo_desc;
    }

    public String getPrioridad_desc() {
        return prioridad_desc;
    }

    public void setPrioridad_desc(String prioridad_desc) {
        this.prioridad_desc = prioridad_desc;
    }

    @Override
    public String toString() {
        return "Reclamo{" +
                "id=" + id +
                ", tipo_cliente=" + tipo_cliente +
                ", tipo_documento=" + tipo_documento +
                ", numero_documento=" + numero_documento +
                ", ape_pat='" + ape_pat + '\'' +
                ", ape_mat='" + ape_mat + '\'' +
                ", nombres='" + nombres + '\'' +
                ", correo='" + correo + '\'' +
                ", ubigeo='" + ubigeo + '\'' +
                ", motivo_reclamo='" + motivo_reclamo + '\'' +
                ", prioridad='" + prioridad + '\'' +
                ", reclamo='" + reclamo + '\'' +
                ", observaciones_comentarios='" + observaciones_comentarios + '\'' +
                '}';
    }
}
