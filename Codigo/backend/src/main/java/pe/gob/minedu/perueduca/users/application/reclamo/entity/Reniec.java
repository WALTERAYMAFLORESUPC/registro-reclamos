package pe.gob.minedu.perueduca.users.application.reclamo.entity;

import javax.persistence.*;


@Entity
@Table(name = "reniec")
public class Reniec {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "dni")
    private int dni;

    @Column(name = "ape_pat")
    private String ape_pat;

    @Column(name = "ape_mat")
    private String ape_mat;

    @Column(name = "nombres")
    private String nombres;

    @Column(name = "ubigeo")
    private String ubigeo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }


    public String getApe_pat() {
        return ape_pat;
    }

    public void setApe_pat(String ape_pat) {
        this.ape_pat = ape_pat;
    }

    public String getApe_mat() {
        return ape_mat;
    }

    public void setApe_mat(String ape_mat) {
        this.ape_mat = ape_mat;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    @Override
    public String toString() {
        return "Reniec{" +
                "id=" + id +
                "dni=" + dni +
                ", ape_pat=" + ape_pat +
                ", ape_mat=" + ape_mat +
                ", nombres=" + nombres +
                ", ubigeo='" + ubigeo + '\'' +
                '}';
    }
}
