package pe.gob.minedu.perueduca.users.application.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class JmsReclamos {
    @Autowired
    private JmsTemplate jmsTemplate;

    @Value("${jms.queue.destination}")
    String nombreCola="";

    public void send(String mensaje){
        jmsTemplate.convertAndSend(nombreCola,mensaje);
    }


}
