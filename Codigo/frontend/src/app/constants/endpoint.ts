import {environment} from "../../environments/environment";

export const endpoint = {

    registro :  environment.api.url +  '/registro-reclamo/save',
    reclamosdni :  environment.api.url +  '/registro-reclamo/reclamos/dni',
    reclamosfecha :  environment.api.url +  '/registro-reclamo/reclamos/fecha',
    regiones : environment.api.url +'/registro-reclamo/regiones',
    provincias : environment.api.url +'/registro-reclamo/provincias',
    distritos : environment.api.url +'/registro-reclamo/distritos',
    reniec : environment.api.url +'/registro-reclamo/reniec',

};