import {Injectable} from '@angular/core';
import * as CryptoJS from 'crypto-js';
import {environment} from "../../environments/environment";


@Injectable({
    providedIn: 'root'
})
export class CryptoService {

    constructor() {
    }

    encrypt(value){
        let nonceToken = CryptoJS.enc.Utf8.parse(environment.cipherKey);
        nonceToken = CryptoJS.enc.Base64.stringify(nonceToken);
        nonceToken = CryptoJS.enc.Base64.parse(nonceToken);
        let encrypt = CryptoJS.AES.encrypt(value, nonceToken, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypt.toString();
    }

}
