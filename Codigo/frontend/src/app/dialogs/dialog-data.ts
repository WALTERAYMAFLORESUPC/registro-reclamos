import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
    selector: 'dialog-success',
    templateUrl: 'dialog-data.html',
})
export class DialogData {
    constructor(
        private dialogRef: MatDialogRef<DialogData>,
        @Inject(MAT_DIALOG_DATA) public data: String) {

    }

    close(){
        this.dialogRef.close();
    }

}