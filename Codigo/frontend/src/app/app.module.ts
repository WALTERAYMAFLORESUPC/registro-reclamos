import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AngularMaterialModule} from './angular-material.module';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from "@angular/flex-layout";
import {LogInComponent} from './components/log-in/log-in.component';
import {RegisterComponent} from './components/register/register.component';
import {MatCardModule} from "@angular/material/card";
import {StorageModule} from '@ngx-pwa/local-storage';
import {HttpClientModule, HttpClientXsrfModule} from "@angular/common/http";
import {HomeComponent} from "./components/home/home.component";
import {DialogData} from "./dialogs/dialog-data";
import {authInterceptorProviders} from "./services/auth.interceptor";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import {NumberDirective} from "./directives/number-input.directive";
import {CryptoService} from '../app/services/crypto.service';
import {RecaptchaModule} from "ng-recaptcha";
import { RegistroReclamoComponent } from './components/registro-reclamo/registro-reclamo.component';
import { ConsultaReclamoComponent } from './components/consulta-reclamo/consulta-reclamo.component';





@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HomeComponent,
    LogInComponent,
    DialogData,
    NumberDirective,
    RegistroReclamoComponent,
    ConsultaReclamoComponent,



  ],
  imports: [
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'XSRF-TOKEN',
      headerName: 'X-XSRF-TOKEN',
    }),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    MatCardModule,
    StorageModule.forRoot({IDBNoWrap: true}),
    RecaptchaModule,
  ],
  providers: [authInterceptorProviders, CryptoService,
    {provide : LocationStrategy , useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [DialogData]
})

export class AppModule { }
