import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegistroReclamoComponent} from './components/registro-reclamo/registro-reclamo.component';
import {ConsultaReclamoComponent} from './components/consulta-reclamo/consulta-reclamo.component';


const routes: Routes = [
  { path: 'registro-reclamo', component: RegistroReclamoComponent },
  { path: 'consulta-reclamo', component: ConsultaReclamoComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
