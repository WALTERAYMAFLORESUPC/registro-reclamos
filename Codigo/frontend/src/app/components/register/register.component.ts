import {Component, OnInit} from '@angular/core';
import {StorageMap} from "@ngx-pwa/local-storage";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {User} from "../../model/user";
import {environment as ENV} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {TokenStorageService} from "../../services/token-storage.service";
import {endpoint} from "../../constants/endpoint";
import {CryptoService} from "../../services/crypto.service";

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

    form: FormGroup;
    user: User;
    course: string;
    courseName: string;
    existError = false;
    errorMessage = '';
    isRegistration: boolean = false;
    showLinkModal: boolean =  true;

    constructor(private storage: StorageMap,
                private router: Router,
                private formBuilder: FormBuilder,
                public dialog: MatDialog,
                private httpClient: HttpClient,
                private tokenStorage: TokenStorageService,
                private cryptoService: CryptoService) {
    }

    ngOnInit() {
       /* this.user = new User();
        this.course =  localStorage.getItem('course');
        this.courseName =  localStorage.getItem('courseName');
        if( localStorage.getItem('registration') == '1' ){
            this.isRegistration =  true;
        }
        this.httpClient
            .get<User>(endpoint.users)
            .subscribe(data => {
                    this.user.firstName = atob(data.firstName);
                    this.user.lastName = atob(data.lastName);
                }, error => {
                    this.router.navigate(['/home']);
                }
            );

        this.form = this.formBuilder.group({
            email: ['', Validators.required],
            phone: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            region: ['00', Validators.required]
        });*/
    }

    get f() { return this.form.controls; }

   /* register() {
        let request = { name: this.cryptoService.encrypt(this.course),
                        phone: this.cryptoService.encrypt(this.f.phone.value),
                        emailAlternative: this.cryptoService.encrypt(this.f.email.value),
                        region: this.cryptoService.encrypt(this.f.region.value)
                        };
        this.httpClient.post<User>(endpoint.register , request)
            .subscribe(data => this.success(data), error => this.error(error.error) )
    }*/

    openDialog() {
        document.getElementById("modalID").click();
    }

    success(response){
        this.openDialog();
    }

    error(error){
        if( error.status == 401 ) {
            this.tokenStorage.signOut();
            this.router.navigate(['/home']);
        }else{
            this.existError =  true;
            this.errorMessage = error.message;
        }
    }

    index(){
        return ENV.redirect;
    }

}
