export class Reclamo{
    tipo_cliente: string;
    tipo_documento: string;
    numero_documento: string;
    ape_pat: string;
    ape_mat: string;
    nombres: string;
    correo: string;
    ubigeo:string;
    motivo_reclamo:string;
    prioridad:string;
    reclamo:string;
    observaciones_comentarios:string;

    ubigeo_:string

    /*descripcciones de id*/
    ubigeo_desc:string;
    motivo_reclamo_desc:string;
    prioridad_desc:string;

}
