import {Component, OnInit} from '@angular/core';
import {Reclamo} from './reclamo'
import {ConsultaReclamoService} from "./consultareclamo.service";
import {ConsultaReclamo} from "./consulta-reclamo";
import {Router} from "@angular/router";
import swal from "sweetalert2";
import {environment as ENV} from "../../../environments/environment";
import {CryptoService} from "../../services/crypto.service";
declare var jQuery:any;
declare var $:any;
@Component({
    selector: 'app-register-user',
    templateUrl: './consulta-reclamo.component.html',
    styleUrls: ['./consulta-reclamo.component.css']
})
export class ConsultaReclamoComponent implements OnInit {

    private consultareclamo: ConsultaReclamo = new ConsultaReclamo();
    reclamos: Reclamo[];
    constructor(private consultaReclamoService: ConsultaReclamoService, private router: Router, private cryptoService: CryptoService) {
    }

    ngOnInit() {


    }

    getReclamos(){
        if(this.consultareclamo.numero_documento!="" && this.consultareclamo.numero_documento!=undefined
         &&(this.consultareclamo.fecha_inicio=="" || this.consultareclamo.fecha_inicio==undefined)
         &&(this.consultareclamo.fecha_fin=="" || this.consultareclamo.fecha_fin==undefined)
         ){
                this.getReclamosPorDni();
        }
        else if(this.consultareclamo.fecha_inicio!="" && this.consultareclamo.fecha_inicio!=undefined
        && this.consultareclamo.fecha_fin!="" && this.consultareclamo.fecha_fin!=undefined
        && (this.consultareclamo.numero_documento=="" || this.consultareclamo.numero_documento==undefined)){
                this.getReclamosPorFecha();
        }
    }
    getReclamosPorDni():void{
                          console.log("abcddd");
                          this.consultaReclamoService.getReclamosPorDni(this.consultareclamo.numero_documento).subscribe(
                          reclamos=> {
                          this.reclamos=reclamos;
                          console.log("reclamos:" + reclamos)
                          }
                          );

                      }
    getReclamosPorFecha():void{
                          console.log("abcddd");
                          this.consultaReclamoService.getReclamosPorFecha(this.consultareclamo.fecha_inicio,this.consultareclamo.fecha_fin).subscribe(
                          reclamos=> {
                          this.reclamos=reclamos;
                          console.log("reclamos:" + reclamos)
                          }
                          );

                      }

    public inputValidatorNumeros(event: any) {
        //console.log(event.target.value);
        const pattern = /^[0-9]*$/;
        //let inputChar = String.fromCharCode(event.charCode)
        if (!pattern.test(event.target.value)) {
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
            // invalid character, prevent input

        }
    }

    public inputValidatorLetras(event: any) {
        //console.log(event.target.value);
        const pattern = /^[a-zA-Z ]*$/;
        //let inputChar = String.fromCharCode(event.charCode)
        if (!pattern.test(event.target.value)) {
            event.target.value = event.target.value.replace(/[^a-zA-Z ]/g, "");
            // invalid character, prevent input

        }
    }

    public inputValidatorLetrasNumeros(event: any) {
        //console.log(event.target.value);
        const pattern = /^[0-9a-zA-Z ]*$/;
        //let inputChar = String.fromCharCode(event.charCode)
        if (!pattern.test(event.target.value)) {
            event.target.value = event.target.value.replace(/[^0-9a-zA-Z ]/g, "");
            // invalid character, prevent input

        }
    }







    siteKey() {
        return ENV.siteKey;
    }



}
