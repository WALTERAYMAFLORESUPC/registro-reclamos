import {Component, OnInit} from '@angular/core';
import {StorageMap} from "@ngx-pwa/local-storage";
import {HttpClient, HttpParams} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {environment as ENV} from "../../../environments/environment";
import {User} from "../../model/user";
import {TokenStorageService} from "../../services/token-storage.service";
import {endpoint} from "../../constants/endpoint";

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  form: any = {};
  course: string;
  courseName: string;
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  isRegistration: boolean = false;
  showLinkModal: boolean =  true;
  showLinkModalLogin: boolean =  true;

  loginForm: FormGroup;
  submitted: boolean = false;
  login: any = {};
  captchaError: boolean = false;

  user: any;

  teacher: string;

  constructor(private storage: StorageMap,
              private httpClient: HttpClient,
              private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,

              private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    this.login.recaptcha = '';
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
    }

    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
    }

    this.route.queryParams.subscribe(params => {
      localStorage.setItem('course', params['course']);
      localStorage.setItem('courseName', params['name']);
      if( params['registration'] == '1' ){
        this.isRegistration =  true;
      }
      localStorage.setItem('registration', params['registration']);

      this.course = params["course"];
      this.courseName = params["name"];
    });

    this.user = this.tokenStorage.getUser();



  }

  onSubmit() {
    if (this.loginForm.invalid) {
      this.isLoginFailed = true;
      this.errorMessage = 'Ingresar usuario y contraseña.';
      return;
    }
    if (this.login.recaptcha.length === 0) {
      this.captchaError = true;
      this.isLoginFailed = true;
      this.errorMessage = 'Seleccionar la casilla de seguridad.';
      return;
    }
    this.login.username = this.loginForm.controls.email.value;
    this.login.password = this.loginForm.controls.password.value;

  }

  success(response){
    localStorage.setItem('userId', response.email);
  }


  successCourse(response){
    this.router.navigate(['/register']);
  }

  /*errorCourse(error){
    if( error.status == 401 ) {
      this.tokenStorage.signOut();
    }else{
      if( error.status === 400 ){
        this.httpClient
            .get<User>(endpoint.users)
            .subscribe(data => {
                  this.teacher = atob(data.firstName) + ' ' + atob(data.lastName);
                }
            );
        document.getElementById("modalIDCourse").click();
      }
    }
  }*/

  reloadPage() {
    window.location.reload();
  }

  index(){
    return ENV.redirect;
  }

  resolved(captchaResponse: string) {
    this.login.recaptcha = captchaResponse;
  }

  siteKey(){
    return ENV.siteKey;
  }

}
