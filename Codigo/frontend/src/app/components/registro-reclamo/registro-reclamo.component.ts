import {Component, OnInit} from '@angular/core';
import {Reclamo} from './reclamo'
import {ReclamoService} from "./reclamo.service";
import {Router} from "@angular/router";
import swal from "sweetalert2";
import {environment as ENV} from "../../../environments/environment";
import {CryptoService} from "../../services/crypto.service";
import { Region } from './region';
import { Persona } from './persona';
declare var jQuery:any;
declare var $:any;
@Component({
    selector: 'app-register-user',
    templateUrl: './registro-reclamo.component.html',
    styleUrls: ['./registro-reclamo.component.css']
})
export class RegistroReclamoComponent implements OnInit {
personaobtenida: Persona={dni: '',ape_pat: '',ape_mat: '',nombres: ''};
    private reclamo: Reclamo = new Reclamo();
    regiones: Region[];
        provincias: Region[];
        distritos: Region[];
    selectedValue = "";
    selectedValueTipoDoc = "";
    check_terminos: boolean = false;
    registerUser: any = {};
    isRegistroFailed = false;
    captchaError: boolean = false;
    errorMessage = '';
 idRegion:"";
    idProvincia:"";
    selectedValueRegiones="";
    selectedValueProvincias=""
    selectedValueDistritos='';

    constructor(private reclamoService: ReclamoService, private router: Router, private cryptoService: CryptoService) {
    }

    ngOnInit() {
        this.reclamo.motivo_reclamo="";
        this.reclamo.prioridad="";
        this.registerUser.recaptcha = '';
        this.reclamo.tipo_documento = '';
        this.selectedValueRegiones='';
        this.selectedValueProvincias='';
        this.selectedValueDistritos='';

        this.reclamoService.getRegiones().subscribe(
                regiones=> this.regiones=regiones
              );

    }

    public inputValidatorNumeros(event: any) {
        //console.log(event.target.value);
        const pattern = /^[0-9]*$/;
        //let inputChar = String.fromCharCode(event.charCode)
        if (!pattern.test(event.target.value)) {
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
            // invalid character, prevent input

        }
    }

    public inputValidatorLetras(event: any) {
        //console.log(event.target.value);
        const pattern = /^[a-zA-Z ]*$/;
        //let inputChar = String.fromCharCode(event.charCode)
        if (!pattern.test(event.target.value)) {
            event.target.value = event.target.value.replace(/[^a-zA-Z ]/g, "");
            // invalid character, prevent input

        }
    }

    public inputValidatorLetrasNumeros(event: any) {
        //console.log(event.target.value);
        const pattern = /^[0-9a-zA-Z ]*$/;
        //let inputChar = String.fromCharCode(event.charCode)
        if (!pattern.test(event.target.value)) {
            event.target.value = event.target.value.replace(/[^0-9a-zA-Z ]/g, "");
            // invalid character, prevent input

        }
    }

    public create(): void {
        console.log("click");
        console.log(this.cryptoService.encrypt(this.reclamo.nombres));

        if (this.registerUser.recaptcha.length === 0) {
            this.captchaError = true;
            this.isRegistroFailed = true;
            this.errorMessage = 'Seleccionar la casilla de seguridad.';
            return;
        }
        if (this.check_terminos) {

             if(
                    this.reclamo.tipo_cliente === undefined || this.reclamo.tipo_cliente === "" ||
                    this.reclamo.tipo_documento === undefined || this.reclamo.tipo_documento === "" ||
                    this.reclamo.numero_documento === undefined || this.reclamo.numero_documento === "" ||
                    this.reclamo.correo === undefined || this.reclamo.correo === "" ||
                    this.reclamo.ubigeo === undefined || this.reclamo.ubigeo === "" ||
                    this.reclamo.motivo_reclamo === undefined || this.reclamo.motivo_reclamo === "" ||
                    this.reclamo.prioridad === undefined || this.reclamo.prioridad === "" ||
                    this.reclamo.reclamo === undefined || this.reclamo.reclamo === "" ||
                    this.reclamo.observaciones_comentarios === undefined || this.reclamo.observaciones_comentarios === ""

                  ){
                                swal.fire('Registro de reclamo', `Debe completar todos los campos.`, 'error');
                  }
                  else{
                  alert("guardando")
                            this.reclamoService.create(this.reclamo)
                               .subscribe(
                                   responseUser => {
                                    console.log("responseUser: " + responseUser);
                                    swal.fire('Registro de reclamo', `Su reclamo se registró correctamente`, 'success');
                                                   }
                                        )

                  }

        } else {
            swal.fire('Aceptar términos', `Debe aceptar los terminos y condiciones`, 'error');
        }
    }


    getPersona():void{
                //alert("getPersona");
                this.personaobtenida={dni: '',ape_pat: '',ape_mat: '',nombres: ''};
                if(this.reclamo.numero_documento.length==8 && this.reclamo.tipo_documento=='1' && this.reclamo.ubigeo_.length==6){
                //alert("getPersona 2");
                $("#ubigeo").blur();
                console.log("abcd");
                this.reclamoService.getPersona(this.reclamo.numero_documento,this.reclamo.ubigeo_).subscribe(
                        campos=> {
                        if(campos!=null){
                        this.personaobtenida=campos;
                        }
                        else{
                        swal.fire('El número de DNI y/o Ubigeo no es correcto. Por favor intente nuevamente.');
                        }
                        console.log("campos:" + this.personaobtenida)
                        }
                        );
                        }
                else{
                this.personaobtenida={dni: '',ape_pat: '',ape_mat: '',nombres: ''};
                }

            }

    resolved(captchaResponse: string) {
        this.registerUser.recaptcha = captchaResponse;
    }

    siteKey() {
        return ENV.siteKey;
    }

     getProvincias():void{
                      console.log("abcddd");
                      this.reclamoService.getProvinciasPorRegion(this.idRegion).subscribe(
                      provincias=> {
                      this.provincias=provincias;
                      console.log("provincias:" + provincias)
                      }
                      );

                  }


                getDistritos():void{
                        console.log("abcd");
                        this.reclamoService.getDistritosPorProvincia(this.idProvincia).subscribe(
                        distritos=> {
                        this.distritos=distritos;
                        console.log("distritos:" + distritos)
                        }
                        );

                    }


}
