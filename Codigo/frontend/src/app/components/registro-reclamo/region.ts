export class Region {

        cod_ubigeo_ie: string;
        distrito: string;
        id_provincia: string;
        provincia: string;
        id_region: string;
        region: string;

}
