import { Injectable } from '@angular/core';
import { endpoint } from "../../constants/endpoint";
import { Reclamo } from './reclamo';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { of, Observable } from 'rxjs';
import { Region } from './region';
import { Persona } from './persona';
@Injectable({
  providedIn: 'root'
})
export class ReclamoService {

      private httpHeaders= new HttpHeaders({'Content-Type':'application/json'});
  constructor(private http: HttpClient) { }

  public create(reclamo: Reclamo): Observable<Reclamo>{
     console.log("creando");
      let request = {
          'tipo_cliente': reclamo.tipo_cliente,
          'tipo_documento': reclamo.tipo_documento,
          'numero_documento': reclamo.numero_documento,
          'ape_pat': reclamo.ape_pat,
          'ape_mat': reclamo.ape_mat,
          'nombres': reclamo.nombres,
          'correo': reclamo.correo,
          'ubigeo': reclamo.ubigeo,
          'motivo_reclamo': reclamo.motivo_reclamo,
          'prioridad': reclamo.prioridad,
          'reclamo': reclamo.reclamo,
          'observaciones_comentarios': reclamo.observaciones_comentarios,
          'ubigeo_': reclamo.ubigeo_,

      }
      return this.http.post<Reclamo>(endpoint.registro, request,{headers: this.httpHeaders});
   }

    public getRegiones(): Observable<Region[]>{
             return this.http.get<Region[]>(endpoint.regiones);
        }

      public getProvinciasPorRegion(idRegion): Observable<Region[]>{
                  return this.http.get<Region[]>(endpoint.provincias+'/'+idRegion);
      }

      public getDistritosPorProvincia(idProvincia): Observable<Region[]>{
                     return this.http.get<Region[]>(endpoint.distritos+'/'+idProvincia);
         }

         public getPersona(dni,ubigeo): Observable<Persona>{
                                       return this.http.get<Persona>(endpoint.reniec+'/'+dni+'/'+ubigeo);
                      }

}
